#!/bin/sh

# Project
# =======
#
# Builds a single Maven project.
#

echo "> Project '$1' ..."

(cd "$1" && mvn install)

echo "< Project '$1'."

