/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2014 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.java8.ex11;

import java.util.stream.IntStream;
import java.util.stream.Stream;

import demo.util.Demo;

/**
 * The demo application.
 */
public class DemoApp {
	
	// constructors
	// ........................................................................

	private DemoApp() {}

	// methods
	// ........................................................................
	
	private void runDemo() {
		
		Demo.log("Running demo ...");
		
		this.demoForEach();
		this.demoFilter();
		this.demoMap();
		this.demoSkip();
		this.demoLimit();
		this.demoDistinct();
		this.demoSorted();
		
		Demo.log("Finished.");
		
	}
	
	private void demoForEach() {
		
		Demo.log("forEach(Consumer):");
		Stream<Integer> stream = this.createStream();
		stream.forEach(x -> Demo.print(x + " "));
		Demo.println(2);
		
	}

	private void demoFilter() {
		
		Demo.log("filter(Predicate):");
		Stream<Integer> stream = this.createStream();
		stream.filter(x -> x % 2 == 0).forEach(x -> Demo.print(x + " "));
		Demo.println(2);
		
	}

	private void demoMap() {
		
		Demo.log("map(Function):");
		Stream<Integer> stream = this.createStream();
		stream.map(x -> 2 * x).forEach(x -> Demo.print(x + " "));
		Demo.println(2);
		
	}

	private void demoSkip() {
		
		Demo.log("skip(int):");
		Stream<Integer> stream = this.createStream();
		stream.skip(3).forEach(x -> Demo.print(x + " "));
		Demo.println(2);
		
	}

	private void demoLimit() {
		
		Demo.log("limit(long):");
		Stream<Integer> stream = this.createStream();
		stream.limit(5).forEach(x -> Demo.print(x + " "));
		Demo.println(2);
		
	}

	private void demoDistinct() {
		
		Demo.log("distinct():");
		Stream<Integer> stream = this.createRandomStream();
		stream.peek(x -> Demo.print(x + "")).distinct().forEach(x -> Demo.print(x + " "));
		Demo.println(2);
		
	}

	private void demoSorted() {
		
		Demo.log("sorted():");
		Stream<Integer> stream = this.createRandomStream();
		stream.sorted().forEach(x -> Demo.print(x + " "));
		Demo.println(2);
		
	}

	private Stream<Integer> createStream() {
		
//		return Stream.of(0, 1, 2, 3, 4, 5, 6, 7, 8, 9);
		
		return IntStream.range(0, 10).boxed();
		
//		return Stream.iterate(0, x -> x + 1).limit(10);
		
//		final List<Integer> list = new ArrayList<>();
//		for (int i = 0; i < 10; i++) {
//			
//			list.add(i);
//			
//		} // for
//		
//		return list.stream();
		
	}

	private Stream<Integer> createRandomStream() {
		
		return Stream.generate(() -> Demo.nextInt(10)).limit(10);
		
//		final List<Integer> list = new ArrayList<>();
//		for (int i = 0; i < 10; i++) {
//			
//			list.add(Demo.nextInt(10));
//			
//		} // for
//		
//		return list.stream();
		
	}

	/**
	 * Runs the program.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {
		
		DemoApp app = new DemoApp();
		app.runDemo();
		
	}
	
}
