/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2014 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.java8.ex50;

import static demo.java8.ex50.Math.add;
import static demo.java8.ex50.Math.cube;
import static demo.java8.ex50.Math.square;

import java.util.concurrent.CompletableFuture;

import demo.util.Demo;

/**
 * The demo application.
 */
public class DemoApp {
	
	// constructors
	// ........................................................................

	private DemoApp() { }

	// methods
	// ........................................................................
	
	/**
	 * Runs the demo.
	 */
	private void runDemo() {
		
		Demo.log("Running demo ...");
		
		try {
			
			this.computeNormally();
			this.computeSequentially();
			this.computeParallelized();
			this.fluentProgramming();
			
			Demo.log("Finished.");
			
		} // try
		catch (Exception e) {
			
			Demo.log(e);
			
		} // catch
		
	}
	
	private void computeNormally() throws Exception {
		
		// x² + x³
		
		Demo.log("Normal Computation:");
		
		long start = System.currentTimeMillis();
		
		Integer result = add(square(2), cube(2));
		
		long end = System.currentTimeMillis();
		
		Demo.log("2² + 2³ = " + result + ", [in " + (end - start) + " ms]");
			
	}
	
	private void computeSequentially() throws Exception {
		
		// x² + x³
		
		Demo.log("Sequential Computation:");
		
		long start = System.currentTimeMillis();
		
		final CompletableFuture<Integer> f0 = new CompletableFuture<>();
		
		final CompletableFuture<Integer> f1 = f0.thenApply(Math::square);
		final CompletableFuture<Integer> f2 = f0.thenApply(Math::cube);
		
		final CompletableFuture<Integer> f3 = f1.thenCombine(f2, Math::add);
		
		f0.complete(2);
		
		Integer result = f3.get();
		
		long end = System.currentTimeMillis();
		
		Demo.log("2² + 2³ = " + result + ", [in " + (end - start) + " ms]");
			
	}
	
	private void computeParallelized() throws Exception {
		
		// x² + x³
		
		Demo.log("Parallel Computation:");
		
		long start = System.currentTimeMillis();
		
		final CompletableFuture<Integer> f0 = new CompletableFuture<>();
		
		final CompletableFuture<Integer> f1 = f0.thenApplyAsync(Math::square);
		final CompletableFuture<Integer> f2 = f0.thenApplyAsync(Math::cube);
		
		final CompletableFuture<Integer> f3 = f1.thenCombine(f2, Math::add);
		
		f0.complete(2);
		
		Integer result = f3.get();
		
		long end = System.currentTimeMillis();
		
		Demo.log("2² + 2³ = " + result + ", [in " + (end - start) + " ms]");
			
	}
	
	private void fluentProgramming() throws Exception {
		
		// x² + x³
		
		Demo.log("Fluent Programming:");
		
		long start = System.currentTimeMillis();
		
		final CompletableFuture<Integer> f0 = new CompletableFuture<>();
		final CompletableFuture<Integer> f3 =
			f0.thenApplyAsync(Math::square)
				.thenCombine(
					f0.thenApplyAsync(Math::cube),
					Math::add
				);
		
		f0.complete(2);
		
		Integer result = f3.get();
		
		long end = System.currentTimeMillis();
		
		Demo.log("2² + 2³ = " + result + ", [in " + (end - start) + " ms]");
			
	}
	
	/**
	 * Runs the program.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {
		
		DemoApp app = new DemoApp();
		app.runDemo();
		
	}
	
}
