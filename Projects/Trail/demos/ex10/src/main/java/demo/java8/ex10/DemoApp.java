/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2014 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.java8.ex10;

import static java.util.Comparator.comparing;
import static java.util.stream.Collectors.groupingBy;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;

import demo.util.Demo;

/**
 * The demo application.
 */
public class DemoApp {
	
	// constructors /////

	private DemoApp() {}
	

	// methods /////
	
	/**
	 * Runs the demo.
	 */
	private void runDemo() {
		
		Demo.log("Running demo ...");
		
		final Map<String, List<Address>> addressesPerCity =
			this.createAddresses()
				.stream()
				.filter(this.citiesBiggerThanOneMillion())
				.sorted(this.compareByCityAndStreet())
				.collect(groupingBy(Address::getCity));
		
		addressesPerCity.entrySet()
			.stream()
			.forEach(
				entry -> this.printCity(entry.getKey(), entry.getValue())
			);
	
		Demo.log("Finished.");
		
	}
	
	private List<Address> createAddresses() {
		
		List<Address> addresses = new ArrayList<>();
		
		addresses.add(new Address("Fraunhoferstrasse",    20, "München"));
		addresses.add(new Address("Kaltmühle",            18, "Frankfurt am Main"));
		addresses.add(new Address("Unter den Linden",    112, "Berlin"));
		addresses.add(new Address("Auguststrasse",        11, "Berlin"));
		addresses.add(new Address("Breite Strasse",       97, "Köln"));
		addresses.add(new Address("Königsallee",          44, "Düsseldorf"));
		addresses.add(new Address("Am Platzl",             1, "München"));
		addresses.add(new Address("Holzstrasse",          49, "München"));
		addresses.add(new Address("Kurfürstendamm",      298, "Berlin"));
		addresses.add(new Address("Hoheluftchaussee",    127, "Hamburg"));
		addresses.add(new Address("Am Römerkastell",      36, "Stuttgart"));
		addresses.add(new Address("Leopoldstrasse",      230, "München"));
		addresses.add(new Address("Akademiegarten",       59, "Stuttgart"));
		addresses.add(new Address("Charlottenplatz",       3, "Stuttgart"));
		addresses.add(new Address("Akazienalle",         120, "Essen"));
		addresses.add(new Address("Brookweg",             29, "Münster"));
		addresses.add(new Address("Potgasse",             12, "Dortmund"));
		addresses.add(new Address("Am Plärrer",            1, "Nürnberg"));
		addresses.add(new Address("Lotterstrasse",        69, "Leipzig"));
		addresses.add(new Address("Postplatz",             5, "Dresden"));
		addresses.add(new Address("Terrassenufer",       223, "Dresden"));
		addresses.add(new Address("An der blauen Kappe",  16, "Augsburg"));
		addresses.add(new Address("Graben",               15, "Weimar"));
		addresses.add(new Address("Wiener Platz",          2, "Dresden"));
		addresses.add(new Address("Spittelmarkt",         55, "Berlin"));
		addresses.add(new Address("Zeil",                103, "Frankfurt am Main"));
		addresses.add(new Address("Kleine Freiheit",      10, "Hamburg"));
		addresses.add(new Address("Barfussgässchen",      19, "Leipzig"));
		addresses.add(new Address("Ruppiner Strasse",     20, "Berlin"));
		addresses.add(new Address("Wiener Platz",          1, "München"));
		
		return addresses;
		
	}
	
	private Predicate<Address> citiesBiggerThanOneMillion() {
		
		final Predicate<Address> berlin   = this.cityBiggerThanOneMillion("Berlin");
		final Predicate<Address> hamburg  = this.cityBiggerThanOneMillion("Hamburg");
		final Predicate<Address> muenchen = this.cityBiggerThanOneMillion("München");
		final Predicate<Address> koeln    = this.cityBiggerThanOneMillion("Köln");
		
		return berlin.or(hamburg).or(muenchen).or(koeln);
		
	}
	
	private Predicate<Address> cityBiggerThanOneMillion(final String city) {
		
		return a -> city.equals(a.getCity());
		
	}
	
	private Comparator<Address> compareByCityAndStreet() {
		
		return comparing(Address::getCity).thenComparing(Address::getStreet);
		
	}
	
	private void printCity(
		final String        city,
		final List<Address> addresses
	) {
		
		Demo.log("%s:", city);
		
		addresses.stream()
			.forEach(a -> Demo.log("  %s %d", a.getStreet(), a.getHouseNo()));
		
	}
	
	/**
	 * Runs the program.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {
		
		DemoApp app = new DemoApp();
		app.runDemo();
		
	}
	
}
