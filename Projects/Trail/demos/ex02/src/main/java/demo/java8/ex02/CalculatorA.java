/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2014 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.java8.ex02;

/**
 * A calculator class.
 */
public class CalculatorA {
	
	// fields
	// ........................................................................
	
	private Operation addition       = (a, b) -> a + b;
	
	private Operation subtraction    = (a, b) -> a - b;
	
	private Operation multiplication = (a, b) -> a * b;
	
	private Operation division       = (a, b) -> a / b;
	
	// constructors
	// ........................................................................
	
	// methods
	// ........................................................................
	
	public int add(int a, int b) { return this.evaluate(addition,       a, b); }
	
	public int sub(int a, int b) { return this.evaluate(subtraction,    a, b); }
	
	public int mlt(int a, int b) { return this.evaluate(multiplication, a, b); }
	
	public int div(int a, int b) { return this.evaluate(division,       a, b); }
	
	private int evaluate(final Operation op, final int a, final int b) {
		
		return op.perform(a, b);
		
	}
	
}
