/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2014 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.java8.ex02;

/**
 * A calculator class.
 */
public class CalculatorB {
	
	// fields
	// ........................................................................
	
	// constructors
	// ........................................................................
	
	// methods
	// ........................................................................
	
	public int add(int a, int b) {
		
		return this.evaluate((x, y) -> x + y, a, b);
		
	}
	
	public int sub(int a, int b) {
		
		return this.evaluate((x, y) -> x - y, a, b);
		
	}
	
	public int mlt(int a, int b) {
		
		return this.evaluate((x, y) -> x * y, a, b);
		
	}
	
	public int div(int a, int b) {
		
		return this.evaluate((x, y) -> x / y, a, b);
		
	}
	
	private int evaluate(Operation operation, int a, int b) {
		
		return operation.perform(a, b);
		
	}
	
}
