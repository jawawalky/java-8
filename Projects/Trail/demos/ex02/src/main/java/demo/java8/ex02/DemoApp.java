/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2014 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.java8.ex02;

import demo.util.Demo;

/**
 * The demo application.
 */
public class DemoApp {
	
	// constructors
	// ........................................................................

	private DemoApp() {}

	// methods
	// ........................................................................
	
	/**
	 * Runs the demo.
	 */
	private void runDemo() {
		
		Demo.log("Running demo ...");
		
		Demo.log("Calculator A:");
		
		CalculatorA calculatorA = new CalculatorA();
		
		Demo.log("3 + 4 = " + calculatorA.add(3, 4));
		Demo.log("3 - 4 = " + calculatorA.sub(3, 4));
		Demo.log("3 * 4 = " + calculatorA.mlt(3, 4));
		Demo.log("3 / 4 = " + calculatorA.div(3, 4));

		
		Demo.log("Calculator B:");
		
		CalculatorB calculatorB = new CalculatorB();
		
		Demo.log("3 + 4 = " + calculatorB.add(3, 4));
		Demo.log("3 - 4 = " + calculatorB.sub(3, 4));
		Demo.log("3 * 4 = " + calculatorB.mlt(3, 4));
		Demo.log("3 / 4 = " + calculatorB.div(3, 4));
		
		
		Demo.log("Calculator C:");
		
		CalculatorC calculatorC = new CalculatorC();
		
		Demo.log("3 + 4 = " + calculatorC.add(3, 4));
		Demo.log("3 - 4 = " + calculatorC.sub(3, 4));
		Demo.log("3 * 4 = " + calculatorC.mlt(3, 4));
		Demo.log("3 / 4 = " + calculatorC.div(3, 4));
		
		Demo.log("Finished.");
		
	}
	
	/**
	 * Runs the program.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {
		
		DemoApp app = new DemoApp();
		app.runDemo();
		
	}
	
}
