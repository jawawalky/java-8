/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2014 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.java8.ex71;

import java.time.Instant;
import java.time.LocalDate;
import java.time.Month;
import java.time.ZoneId;

import demo.util.Demo;

/**
 * The demo application.
 */
public class DemoApp {
	
	// constructors
	// ........................................................................

	private DemoApp() {}

	// methods
	// ........................................................................
	
	/**
	 * Runs the demo.
	 */
	private void runDemo() {
		
		Demo.log("Running demo ...");
		
		LocalDate d1 = LocalDate.now();
		Demo.log(d1.toString());
		
		LocalDate d2 = LocalDate.parse("2013-06-15");
		Demo.log(d2.toString());
		
		LocalDate d3 = LocalDate.of(2000, Month.AUGUST, 17);
		Demo.log(d3.toString());
		
		Instant i1 = d1.atStartOfDay().atZone(ZoneId.of("America/Chicago")).toInstant();
		Instant i2 = d1.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant();
		Instant i3 = d1.atStartOfDay().atZone(ZoneId.of("Asia/Singapore")).toInstant();
		
		Demo.log(i1.toString());
		Demo.log(i2.toString());
		Demo.log(i3.toString());
		
		Demo.log(ZoneId.of("America/Chicago") + " is after " + ZoneId.systemDefault() + ": " + i1.isAfter(i2));
		Demo.log(ZoneId.systemDefault() + " is after " + ZoneId.of("Asia/Singapore") + ": " + i2.isAfter(i3));
		
		Demo.log("Finished.");
		
	}
	
	/**
	 * Runs the program.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {
		
		DemoApp app = new DemoApp();
		app.runDemo();
		
	}
	
}
