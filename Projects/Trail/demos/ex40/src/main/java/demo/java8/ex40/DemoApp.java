/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2016 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.java8.ex40;

import static java.util.stream.Collectors.*;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import demo.util.Demo;

/**
 * The demo application.
 */
public class DemoApp {
	
	// constructors
	// ........................................................................

	private DemoApp() {}

	// methods
	// ........................................................................
	
	private void runDemo() {
		
		Demo.log("Running demo ...");
		
		this.demoParallelSort();
		
		Demo.log("Finished.");
		
	}
	
	private void demoParallelSort() {
		
		Demo.log("Parallel sorting");
		
		Integer[] array = this.createArray(100000);
		
		Demo.log("  Before sorting: " + this.firstElements(array, 10));
		Arrays.parallelSort(array);
		Demo.log("  After sorting:  " + this.firstElements(array, 10));
		
	}
	
	private Integer[] createArray(final int size) {
		
		Integer[] array = new Integer[size];
		
		for (int i = 0; i < size; i++) {
			
			array[i] = Demo.nextInt(size);
			
		} // for
		
		return array;
		
	}
	
	private <T> String firstElements(final T[] array, final int n) {
		
		return Arrays.stream(array).limit(n).map(e -> e.toString()).collect(joining(", "));
		
	}
	
	/**
	 * Runs the program.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {
		
		DemoApp app = new DemoApp();
		app.runDemo();
		
	}
	
}
