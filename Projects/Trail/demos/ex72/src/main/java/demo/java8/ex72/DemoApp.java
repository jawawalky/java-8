/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2014 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.java8.ex72;

import java.time.LocalDate;
import java.time.Period;
import java.time.temporal.ChronoUnit;

import demo.util.Demo;

/**
 * The demo application.
 */
public class DemoApp {
	
	// constructors
	// ........................................................................

	private DemoApp() {}

	// methods
	// ........................................................................
	
	/**
	 * Runs the demo.
	 */
	private void runDemo() {
		
		Demo.log("Running demo ...");
		
		String birthday = Demo.input("Enter the date of your birthday in the format YYYY-MM-DD");
		
		LocalDate dateOfBirth = LocalDate.parse(birthday);
		LocalDate today       = LocalDate.now();
		
		Period period = Period.between(dateOfBirth, today);
		long   days   = ChronoUnit.DAYS.between(dateOfBirth, today);
		
		Demo.log(
			"You are %d years, %d months, and %d days old. (%d days total)",
            period.getYears(),
            period.getMonths(),
            period.getDays(),
            days
		);
		
		Demo.log("Finished.");
		
	}
	
	/**
	 * Runs the program.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {
		
		DemoApp app = new DemoApp();
		app.runDemo();
		
	}
	
}
