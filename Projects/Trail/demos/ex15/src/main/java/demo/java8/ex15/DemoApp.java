/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2014 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.java8.ex15;

import java.util.stream.IntStream;

import demo.util.Demo;
import demo.util.StopWatch;

/**
 * The demo application.
 */
public class DemoApp {
	
	// constructors
	// ........................................................................

	private DemoApp() {}

	// methods
	// ........................................................................
	
	private void runDemo() {
		
		Demo.log("Running demo ...");

		this.demoSequential();
		this.demoParallel();
		
		Demo.log("Finished.");
		
	}
	
	private void demoSequential() {

		Demo.log("Sequential:");
		
		StopWatch stopWatch = StopWatch.simpleStopWatch();
		stopWatch.start();
		
		IntStream
			.generate(() -> Demo.nextInt(10))
			.limit(1000)
			.peek(e -> Demo.sleep(5))
			.sum();
		
		stopWatch.stop();
		stopWatch.print();
		
	}
	
	private void demoParallel() {

		Demo.log("Parallel:");
		
		StopWatch stopWatch = StopWatch.simpleStopWatch();
		stopWatch.start();
		
		IntStream
			.generate(() -> Demo.nextInt(10))
			.limit(1000)
			.parallel()
			.peek(e -> Demo.sleep(5))
			.sum();
		
		stopWatch.stop();
		stopWatch.print();
		
	}
	
	/**
	 * Runs the program.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {
		
		DemoApp app = new DemoApp();
		app.runDemo();
		
	}
	
}
