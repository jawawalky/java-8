/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2014 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.java8.ex70;

import java.time.Duration;
import java.time.Instant;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;

import demo.util.Demo;

/**
 * The demo application.
 */
public class DemoApp {
	
	// constructors
	// ........................................................................

	private DemoApp() {}

	// methods
	// ........................................................................
	
	/**
	 * Runs the demo.
	 */
	private void runDemo() {
		
		Demo.log("Running demo ...");
		
		Instant i1 = Instant.now();
		Demo.log(i1.toString());
		
		Instant i2 = i1.minus(5, ChronoUnit.DAYS).plus(3, ChronoUnit.HOURS);
		Demo.log(i2.toString());
		
		Instant i3 = i1.minus(Duration.ofDays(5)).plus(Duration.ofHours(3));
		Demo.log(i3.toString());
		
		Demo.log(i1.atZone(ZoneId.of("America/Chicago")).toString());
		Demo.log(i1.atZone(ZoneId.of("Asia/Singapore")).toString());
		
		Demo.log("Finished.");
		
	}
	
	/**
	 * Runs the program.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {
		
		DemoApp app = new DemoApp();
		app.runDemo();
		
	}
	
}
