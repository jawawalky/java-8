/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2014 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.java8.ex16;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.groupingByConcurrent;

import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import demo.util.Demo;
import demo.util.StopWatch;

/**
 * The demo application.
 */
public class DemoApp {
	
	// constructors
	// ........................................................................

	private DemoApp() {}

	// methods
	// ........................................................................
	
	private void runDemo() {
		
		Demo.log("Running demo ...");

		this.demoSequential();
		this.demoParallel();
		
		Demo.log("Finished.");
		
	}
	
	private void demoSequential() {

		Demo.log("Sequential:");
		
		StopWatch stopWatch = StopWatch.simpleStopWatch();
		stopWatch.start();
		
		Map<Integer, List<Integer>> result =
			Stream
				.generate(() -> Demo.nextInt(12))
				.limit(1000)
				.peek(e -> Demo.sleep(5))
				.collect(groupingBy((Integer e) -> e % 3));
		
		Demo.log("0: %d", result.get(0).size());
		Demo.log("1: %d", result.get(1).size());
		Demo.log("2: %d", result.get(2).size());
		
		stopWatch.stop();
		stopWatch.print();
		
	}
	
	private void demoParallel() {

		Demo.log("Parallel:");
		
		StopWatch stopWatch = StopWatch.simpleStopWatch();
		stopWatch.start();
		
		
		Map<Integer, List<Integer>> result =
			Stream
				.generate(() -> Demo.nextInt(12))
				.limit(1000)
				.parallel()
				.peek(e -> Demo.sleep(5))
				.collect(groupingByConcurrent((Integer e) -> e % 3));
		
		Demo.log("0: %d", result.get(0).size());
		Demo.log("1: %d", result.get(1).size());
		Demo.log("2: %d", result.get(2).size());
		
		stopWatch.stop();
		stopWatch.print();
		
	}
	
	/**
	 * Runs the program.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {
		
		DemoApp app = new DemoApp();
		app.runDemo();
		
	}
	
}
