/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2016 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.java8.ex55;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.StampedLock;

import demo.util.Demo;

/**
 * A translator.
 * 
 * @author Franz Tost
 */
public class Translator {
	
	// constants
	// ........................................................................
	
	private static final Map<String, String> DICTIONARY = new HashMap<>();

	// static block
	// ........................................................................
	
	static {
		
		DICTIONARY.put("rain", "Regen");
		DICTIONARY.put("sun",  "Sonne");
		DICTIONARY.put("wind", "Wind");
		DICTIONARY.put("snow", "Schnee");
		
	}
	
	// fields
	// ........................................................................
	
	private StampedLock lock = new StampedLock();

	// methods
	// ........................................................................

	public String translate(final String wordInEnglish) {
		
		long startTime = System.currentTimeMillis();
		
		long stamp = this.lock.tryOptimisticRead();
		
		Demo.sleep(500);
		String wordInGerman = DICTIONARY.get(wordInEnglish);
		
		if (!this.lock.validate(stamp)) {
			
			try {
				
				stamp = this.lock.readLock();
				Demo.sleep(500);
				wordInGerman = DICTIONARY.get(wordInEnglish);
				
			} // try
			finally {
				
				this.lock.unlock(stamp);
				
			} // finally
			
		} // if
		
		Demo.log("translate(): " + wordInEnglish + " -> " + wordInGerman + " [" + (System.currentTimeMillis() - startTime) + " ms]");
		return wordInGerman;
		
	}
	
	public void addTranslation(
		final String wordInEnglish,
		final String wordInGerman
	) {
		
		long startTime = System.currentTimeMillis();
		
		long stamp = this.lock.writeLock();
		
		try {
			
			Demo.sleep(2000);
			DICTIONARY.put(wordInEnglish, wordInGerman);
			
		} // try
		finally {
			
			this.lock.unlock(stamp);
			
		}
		
		Demo.log("addTranslation: " + wordInEnglish + " -> " + wordInGerman +  " [" + (System.currentTimeMillis() - startTime) + " ms]");
		
	}
	
}
