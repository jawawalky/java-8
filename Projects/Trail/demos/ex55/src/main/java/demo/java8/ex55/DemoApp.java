/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2016 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.java8.ex55;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import demo.util.Demo;

/**
 * The demo application.
 */
public class DemoApp {
	
	// constructors
	// ........................................................................

	/**
	 * A private default constructor.
	 */
	private DemoApp() {
	}

	// methods
	// ........................................................................
	
	/**
	 * Runs the demo.
	 */
	private void runDemo() {
		
		Demo.log("Running demo ...");
		
		this.testA();
		this.testB();
		this.testC();
		this.testD();
		this.testE();
		this.testF();
		
		Demo.terminate(0);
		
	}
	
	private void testA() {
		
		Demo.log("Test A ...");
		
		try {
			
			Executor executor = Executors.newCachedThreadPool();
			
			Translator translator = new Translator();
			
			executor.execute(() -> translator.translate("rain"));
			
			Demo.sleep(1500);
			
			executor.execute(() -> translator.addTranslation("fog", "Nebel"));
			
			Demo.sleep(5000);
			
		} // try
		catch (Exception e) {
			
			Demo.log(e);
			
		} // catch
		
	}
	
	private void testB() {
		
		Demo.log("Test B ...");
		
		try {
			
			Executor executor = Executors.newCachedThreadPool();
			
			Translator translator = new Translator();
			
			executor.execute(() -> translator.translate("rain"));
			
			Demo.sleep(250);
			
			executor.execute(() -> translator.addTranslation("fog", "Nebel"));
			
			Demo.sleep(5000);
			
		} // try
		catch (Exception e) {
			
			Demo.log(e);
			
		} // catch
		
	}
	
	private void testC() {
		
		Demo.log("Test C ...");
		
		try {
			
			Executor executor = Executors.newCachedThreadPool();
			
			Translator translator = new Translator();
			
			executor.execute(() -> translator.addTranslation("fog", "Nebel"));
			
			Demo.sleep(250);
			
			executor.execute(() -> translator.translate("rain"));
			
			Demo.sleep(5000);
			
		} // try
		catch (Exception e) {
			
			Demo.log(e);
			
		} // catch
		
	}
	
	private void testD() {
		
		Demo.log("Test D ...");
		
		try {
			
			Executor executor = Executors.newCachedThreadPool();
			
			Translator translator = new Translator();
			
			executor.execute(() -> translator.addTranslation("fog", "Nebel"));
			
			Demo.sleep(2500);
			
			executor.execute(() -> translator.translate("rain"));
			
			Demo.sleep(5000);
			
		} // try
		catch (Exception e) {
			
			Demo.log(e);
			
		} // catch
		
	}
	
	private void testE() {
		
		Demo.log("Test E ...");
		
		try {
			
			Executor executor = Executors.newCachedThreadPool();
			
			Translator translator = new Translator();
			
			executor.execute(() -> translator.translate("snow"));
			
			Demo.sleep(750);
			
			executor.execute(() -> translator.translate("rain"));
			
			Demo.sleep(5000);
			
		} // try
		catch (Exception e) {
			
			Demo.log(e);
			
		} // catch
		
	}
	
	private void testF() {
		
		Demo.log("Test F ...");
		
		try {
			
			Executor executor = Executors.newCachedThreadPool();
			
			Translator translator = new Translator();
			
			executor.execute(() -> translator.translate("snow"));
			
			Demo.sleep(250);
			
			executor.execute(() -> translator.translate("rain"));
			
			Demo.sleep(5000);
			
		} // try
		catch (Exception e) {
			
			Demo.log(e);
			
		} // catch
		
	}
	
	/**
	 * Runs the program.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {
		
		DemoApp app = new DemoApp();
		app.runDemo();
		
	}
	
}
