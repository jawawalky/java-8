package demo.java8.ex20;

/**
 * A cube operation.
 */
public class Cube implements Operation<Integer> {
	
	// methods
	// ........................................................................
	
	@Override
	public Integer apply(final Integer value) {
		
		return value * value * value;

	}

}
