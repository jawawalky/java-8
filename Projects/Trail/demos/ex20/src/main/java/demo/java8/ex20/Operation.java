package demo.java8.ex20;

/**
 * An interface, which can be used for fluent programming.
 *
 * @param <T> the operation type.
 */
@FunctionalInterface
public interface Operation<T> {
	
	// methods
	// ........................................................................
	
	T apply(T target);
	
	default Operation<T> andThen(Operation<T> other) {
		
		return v -> other.apply(this.apply(v));
		
	}
	
}
