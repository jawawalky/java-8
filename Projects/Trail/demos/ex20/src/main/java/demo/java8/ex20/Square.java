package demo.java8.ex20;

/**
 * A square operation.
 */
public class Square implements Operation<Integer> {
	
	// methods
	// ........................................................................
	
	@Override
	public Integer apply(final Integer value) {
		
		return value * value;

	}

}
