/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2014 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.java8.ex21;

import java.util.function.UnaryOperator;

import demo.util.Demo;

/**
 * The demo application.
 */
public class DemoApp {
	
	// constructors /////

	private DemoApp() {}

	
	// methods /////
	
	/**
	 * Runs the demo.
	 */
	private void runDemo() {
		
		Demo.log("Running demo ...");
		
		UnaryOperator<Integer> square = x -> x * x;
		UnaryOperator<Integer> cube   = x -> x * x * x;
		
		Demo.log("Result: " + square.andThen(cube).apply(2));
		
		Demo.log("Finished.");
		
	}
	
	/**
	 * Runs the program.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {
		
		DemoApp app = new DemoApp();
		app.runDemo();
		
	}
	
}
