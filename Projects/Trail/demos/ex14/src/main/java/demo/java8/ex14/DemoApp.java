/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2014 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.java8.ex14;

import java.util.stream.IntStream;
import java.util.stream.Stream;

import demo.util.Demo;

/**
 * The demo application.
 */
public class DemoApp {
	
	// constructors
	// ........................................................................

	private DemoApp() {}

	// methods
	// ........................................................................
	
	private void runDemo() {
		
		Demo.log("Running demo ...");

		this.demoSequential();
		this.demoParallel();
		this.demoSequentialWithExecutingThread();
		this.demoParallelWithExecutingThread();
//		this.demoParallelSort();
		
		Demo.log("Finished.");
		
	}
	
	private void demoSequential() {
		
		Demo.log("Sequential:");
		this.createStream().forEach(x -> Demo.print(x + " "));
		Demo.println(2);
		
	}
	
	private void demoParallel() {
		
		Demo.log("Parallel:");
		this.createStream().parallel().forEach(x -> Demo.print(x + " "));
		Demo.println(2);
		
	}
	
	private void demoSequentialWithExecutingThread() {
		
		Demo.log("Sequential with Thread-Info:");
		this.createStream()
			.forEach(x -> Demo.println("[" + Thread.currentThread().getName() + "]: " + x.toString()));
		Demo.println(2);
		
	}
	
	private void demoParallelWithExecutingThread() {
		
		Demo.log("Parallel with Thread-Info:");
		this.createStream()
			.parallel()
			.forEach(x -> Demo.println("[" + Thread.currentThread().getName() + "]: " + x.toString()));
		Demo.println(2);
		
	}
	
	private void demoParallelSort() {
		
		Demo.log("Parallel Sort:");
		this.createRandomStream(1000)
			.parallel()
			.sorted()
			.forEachOrdered(x -> Demo.println(x.toString()));
		Demo.println(2);
		
	}
	
	private Stream<Integer> createStream() {
		
		return IntStream.range(0, 10).boxed();
		
	}

	private Stream<Integer> createRandomStream(final int limit) {
		
		return Stream.generate(() -> Demo.nextInt(10)).limit(limit);
		
	}

	/**
	 * Runs the program.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {
		
		DemoApp app = new DemoApp();
		app.runDemo();
		
	}
	
}
