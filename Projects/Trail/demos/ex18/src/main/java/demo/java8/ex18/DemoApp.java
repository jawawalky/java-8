/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2014 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.java8.ex18;

import java.util.stream.IntStream;

import demo.util.Demo;

/**
 * The demo application.
 */
public class DemoApp {
	
	// constructors
	// ........................................................................

	private DemoApp() {}

	// methods
	// ........................................................................
	
	private void runDemo() {
		
		Demo.log("Running demo ...");
		
		this.demoOrderOfOperations();
		this.demoCallingStrategy();
		
		Demo.log("Finished.");
		
	}

	/**
	 * Check, if the order of the operations is important.
	 */
	private void demoOrderOfOperations() {
		
		Demo.log("Order of Operations:");

		
		Demo.log("A)");
		
		IntStream
			.of(33, 44, 11, 55, 22, 77, 66, 88)
			.sorted()
			.skip(2)
			.limit(4)
			.forEach(x -> Demo.print(x + " "));
		Demo.println(2);

		
		Demo.log("B)");
		
		IntStream
			.of(33, 44, 11, 55, 22, 77, 66, 88)
			.skip(2)
			.limit(4)
			.sorted()
			.forEach(x -> Demo.print(x + " "));
		Demo.println(2);
		
	}

	/**
	 * Discover, how stream operations are called.
	 */
	private void demoCallingStrategy() {
		
		Demo.log("Calling Strategy:");

		
		Demo.log("A)");
		
		IntStream
			.of(33, 44, 11, 55, 22, 77, 66, 88)
			.filter(x -> { Demo.print("a"); return true; })
			.filter(x -> { Demo.print("b"); return true; })
			.forEach(x -> Demo.print(":" + x + " "));
		Demo.println(2);

		
		Demo.log("B)");
		
		IntStream
			.of(33, 44, 11, 55, 22, 77, 66, 88)
			.filter(x -> { Demo.print("a"); return true; })
			.filter(x -> { Demo.print("b"); return true; })
			.map(x -> { Demo.print("c"); return x; })
			.forEach(x -> Demo.print(":" + x + " "));
		Demo.println(2);

		
		Demo.log("C)");
		
		IntStream
			.of(33, 44, 11, 55, 22, 77, 66, 88)
			.filter(x -> { Demo.print("a"); return true; })
			.filter(x -> { Demo.print("b"); return true; })
			.skip(3)
			.limit(4)
			.forEach(x -> Demo.print(":" + x + " "));
		Demo.println(2);

		
		Demo.log("D)");
		
		IntStream
			.of(33, 44, 11, 55, 22, 77, 66, 88)
			.filter(x -> { Demo.print("a"); return true; })
			.filter(x -> { Demo.print("b"); return true; })
			.sorted()
			.forEach(x -> Demo.print(":" + x + " "));
		Demo.println(2);

		
	}

	/**
	 * Runs the program.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {
		
		DemoApp app = new DemoApp();
		app.runDemo();
		
	}
	
}
