/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2014 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.java8.ex05;

/**
 * A class representing an address.
 */
public class Address {
	
	// fields
	// ........................................................................
	
	private String street;
	
	private int houseNo;
	
	private String city;
			
	// constructors
	// ........................................................................
	
	public Address(
		final String street,
		final int    houseNo,
		final String city
	) {
		
		this.street  = street;
		this.houseNo = houseNo;
		this.city    = city;
		
	}
	
	// methods
	// ........................................................................
	
	public String toString() {
		
		StringBuilder sb = new StringBuilder();
		
		sb.append(this.street).append(" ").append(this.houseNo).append(", ").append(this.city);
		
		return sb.toString();
		
	}

	public String getStreet() {
		
		return this.street;
		
	}

	public int getHouseNo() {
		
		return this.houseNo;
		
	}

	public String getCity() {
		
		return this.city;
		
	}
	
	public static int compareByStreet(final Address address1, final Address address2) {
		
		return address1.getStreet().compareTo(address2.getStreet());
		
	}

	public static int compareByCity(final Address address1, final Address address2) {
		
		return address1.getCity().compareTo(address2.getCity());
		
	}

}
