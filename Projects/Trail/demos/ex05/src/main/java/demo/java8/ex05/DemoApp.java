/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2014 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.java8.ex05;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import demo.util.Demo;

/**
 * The demo application.
 */
public class DemoApp {
	
	// constructors
	// ........................................................................

	private DemoApp() {}

	// methods
	// ........................................................................
	
	/**
	 * Runs the demo.
	 */
	private void runDemo() {
		
		Demo.log("Running demo ...");
		
		this.printAddressesSortedByStreetWithAnonymousClass();
		this.printAddressesSortedByStreetWithLambdaExpression();
		this.printAddressesSortedByStreetWithMethodReference();
		this.printAddressesSortedByCityWithMethodReference();
		
		Demo.log("Finished.");
		
	}
	
	private List<Address> createAddresses() {
		
		List<Address> addresses = new ArrayList<>();
		
		addresses.add(new Address("Fraunhoferstrasse",    20, "München"));
		addresses.add(new Address("Kaltmühle",            18, "Frankfurt am Main"));
		addresses.add(new Address("Unter den Linden",    112, "Berlin"));
		addresses.add(new Address("Auguststrasse",        11, "Berlin"));
		addresses.add(new Address("Breite Strasse",       97, "Köln"));
		addresses.add(new Address("Königsallee",          44, "Düsseldorf"));
		addresses.add(new Address("Am Platzl",             1, "München"));
		addresses.add(new Address("Holzstrasse",          49, "München"));
		addresses.add(new Address("Kurfürstendamm",      298, "Berlin"));
		addresses.add(new Address("Hoheluftchaussee",    127, "Hamburg"));
		addresses.add(new Address("Am Römerkastell",      36, "Stuttgart"));
		addresses.add(new Address("Leopoldstrasse",      230, "München"));
		addresses.add(new Address("Akademiegarten",       59, "Stuttgart"));
		addresses.add(new Address("Charlottenplatz",       3, "Stuttgart"));
		addresses.add(new Address("Akazienalle",         120, "Essen"));
		addresses.add(new Address("Brookweg",             29, "Münster"));
		addresses.add(new Address("Potgasse",             12, "Dortmund"));
		addresses.add(new Address("Am Plärrer",            1, "Nürnberg"));
		addresses.add(new Address("Lotterstrasse",        69, "Leipzig"));
		addresses.add(new Address("Postplatz",             5, "Dresden"));
		addresses.add(new Address("Terrassenufer",       223, "Dresden"));
		addresses.add(new Address("An der blauen Kappe",  16, "Augsburg"));
		addresses.add(new Address("Graben",               15, "Weimar"));
		addresses.add(new Address("Wiener Platz",          2, "Dresden"));
		addresses.add(new Address("Spittelmarkt",         55, "Berlin"));
		addresses.add(new Address("Zeil",                103, "Frankfurt am Main"));
		addresses.add(new Address("Kleine Freiheit",      10, "Hamburg"));
		addresses.add(new Address("Barfussgässchen",      19, "Leipzig"));
		addresses.add(new Address("Ruppiner Strasse",     20, "Berlin"));
		addresses.add(new Address("Wiener Platz",          1, "München"));
		
		return addresses;
		
	}
	
	private void printAddresses(final List<Address> addresses) {
		
		for(Address address : addresses) {
			
			Demo.log(address.toString());
			
		} // for
		
	}
		
	private void printAddressesSortedByStreetWithAnonymousClass() {
		
		Demo.log("(1) Addresses Sorted by Street with Anonymous Class:");
		
		List<Address> addresses = this.createAddresses();
		Collections.sort(
			addresses,
			new Comparator<Address>() {
				@Override
				public int compare(final Address address1, final Address address2) {
					return Address.compareByStreet(address1, address2);
				}
			}
		);
		
		this.printAddresses(addresses);
		
	}
	
	private void printAddressesSortedByStreetWithLambdaExpression() {
		
		Demo.log("(2) Addresses Sorted by Street with Lambda Expression:");
		
		List<Address> addresses = this.createAddresses();
		Collections.sort(
			addresses,
			(a, b) -> Address.compareByStreet(a, b)
		);
		
		this.printAddresses(addresses);
		
	}
	
	private void printAddressesSortedByStreetWithMethodReference() {
		
		Demo.log("(3) Addresses Sorted by Street with Method Reference:");
		
		List<Address> addresses = this.createAddresses();
		Collections.sort(
			addresses,
			Address::compareByStreet
		);
		
		this.printAddresses(addresses);
		
	}
	
	private void printAddressesSortedByCityWithMethodReference() {
		
		Demo.log("(4) Addresses Sorted by City with Method Reference:");
		
		List<Address> addresses = this.createAddresses();
		Collections.sort(
			addresses,
			Address::compareByCity
		);
		
		this.printAddresses(addresses);
		
	}
	
	/**
	 * Runs the program.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {
		
		DemoApp app = new DemoApp();
		app.runDemo();
		
	}
	
}
