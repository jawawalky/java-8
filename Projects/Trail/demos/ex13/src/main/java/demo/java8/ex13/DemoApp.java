/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2014 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.java8.ex13;

import static demo.java8.ex13.ABC.A;
import static demo.java8.ex13.ABC.B;
import static demo.java8.ex13.ABC.C;
import static demo.java8.ex13.NumberType.EVEN;
import static demo.java8.ex13.NumberType.ODD;
import static java.util.stream.Collectors.averagingInt;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.partitioningBy;
import static java.util.stream.Collectors.toSet;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Stream;

import demo.util.Demo;

/**
 * The demo application.
 */
public class DemoApp {
	
	// constructors
	// ........................................................................

	private DemoApp() {}

	// methods
	// ........................................................................
	
	private void runDemo() {
		
		Demo.log("Running demo ...");

		this.demoJoining();
		this.demoGroupingBy();
		this.demoGroupingBy2();
		this.demoPartitioningBy();
		this.demoGroupingBy3();
		this.demoToSet();
		this.demoAveragingInt();
		
		Demo.log("Finished.");
		
	}
	
	private void demoJoining() {
		
		Demo.log("joining(CharSequence):");
		String line = Stream.of("one", "two", "three").collect(joining(", "));
		Demo.println(line);
		Demo.println(2);
		
	}
	
	private void demoGroupingBy() {
		
		Demo.log("groupingBy(Function):");
		Map<NumberType, List<Integer>> map =
			this.createStream()
				.collect(groupingBy(x -> (x % 2 == 0 ? EVEN : ODD)));
		Demo.print("ODD:  ");
		map.get(ODD).stream().forEach(x -> Demo.print(x + " "));
		Demo.println();
		Demo.print("EVEN: ");
		map.get(EVEN).stream().forEach(x -> Demo.print(x + " "));
		Demo.println(2);
		
	}
	
	private void demoGroupingBy2() {
		
		Demo.log("groupingBy(Function):");
		Map<Boolean, List<Integer>> map =
			this.createStream()
				.collect(groupingBy(x -> (x % 2 == 0)));
		Demo.print("ODD:  ");
		map.get(Boolean.FALSE).stream().forEach(x -> Demo.print(x + " "));
		Demo.println();
		Demo.print("EVEN: ");
		map.get(Boolean.TRUE).stream().forEach(x -> Demo.print(x + " "));
		Demo.println(2);
		
	}
	
	private void demoPartitioningBy() {
		
		Demo.log("partitioningBy(Predicate):");
		Map<Boolean, List<Integer>> map =
			this.createStream()
				.collect(partitioningBy(x -> (x % 2 == 0)));
		Demo.print("ODD:  ");
		map.get(Boolean.FALSE).stream().forEach(x -> Demo.print(x + " "));
		Demo.println();
		Demo.print("EVEN: ");
		map.get(Boolean.TRUE).stream().forEach(x -> Demo.print(x + " "));
		Demo.println(2);
		
	}
	
	private void demoGroupingBy3() {
		
		Demo.log("groupingBy(Function):");
		Map<ABC, List<Integer>> map =
			this.createStream()
				.collect(groupingBy(x -> ABC.values()[x % 3]));
		Demo.print("A: ");
		map.get(A).stream().forEach(x -> Demo.print(x + " "));
		Demo.println();
		Demo.print("B: ");
		map.get(B).stream().forEach(x -> Demo.print(x + " "));
		Demo.println();
		Demo.print("C: ");
		map.get(C).stream().forEach(x -> Demo.print(x + " "));
		Demo.println(2);
		
	}
	
	private void demoToSet() {
		
		Demo.log("toSet():");
		Set<Integer> set = this.createRandomStream().collect(toSet());
		set.stream().forEach(x -> Demo.print(x + " "));
		Demo.println(2);
		
	}
	
	private void demoAveragingInt() {
		
		Demo.log("averagingInt(ToIntFunction):");
		Double value = this.createRandomStream().collect(averagingInt(x -> x));
		Demo.print("Average: " + value);
		Demo.println(2);
		
	}
	
	private Stream<Integer> createStream() {
		
		return Stream.of(0, 1, 2, 3, 4, 5, 6, 7, 8, 9);
		
	}

	private Stream<Integer> createRandomStream() {
		
		return Stream.generate(() -> Demo.nextInt(10)).limit(10);
		
	}

	/**
	 * Runs the program.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {
		
		DemoApp app = new DemoApp();
		app.runDemo();
		
	}
	
}
