/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2014 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.java8.ex70;

import demo.util.Demo;

/**
 * The demo application.
 */
public class DemoApp {
	
	// constructors
	// ........................................................................

	private DemoApp() {}

	// methods
	// ........................................................................
	
	/**
	 * Runs the demo.
	 */
	private void runDemo() {
		
		Demo.log("Running demo ...");
		
		// TODO
		//
		//  o Create an instance 'i1' of 'java.time.Instant' containing
		//    the current time. Print the instant on the console.
		//
		//  o Derive an instant, which lies 5 days in the past, but three
		//    hours after the current hour. Print the instant on the console.
		//
		//  o Derive the same instant, using the class 'java.time.Duration'.
		//    Print the instant on the console.
		//
		//  o Print the time, which corresponds to the instant 'i1' in
		//    the time zone with the ID 'America/Chicago'.
		//
		//  o Print the time, which corresponds to the instant 'i1' in
		//    the time zone with the ID 'Asia/Singapore'.
		//
		
		Demo.log("Finished.");
		
	}
	
	/**
	 * Runs the program.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {
		
		DemoApp app = new DemoApp();
		app.runDemo();
		
	}
	
}
