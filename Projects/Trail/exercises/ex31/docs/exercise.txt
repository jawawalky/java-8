Default Interface Methods
=========================

A) Implementing a Default Interface Method
==========================================

  'Scalar':
  ---------
  
  1) Convert the static power method into a default method of the interface.
  
  2) Change the signature of the method, so the value 'x' becomes the scalar,
     on which the method is invoked.
     
     
B) Using a Default Interface Method
===================================

  'DemoApp':
  ----------
  
  'runDemo()':
  ------------
  
  1) Change the code, so the method is invoked on the variable 'x'.
