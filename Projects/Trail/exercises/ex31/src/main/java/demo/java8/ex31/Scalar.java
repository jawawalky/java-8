/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2016 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.java8.ex31;

/**
 * An interface with a static method.
 * 
 * @author Franz Tost
 */
public interface Scalar<T extends Scalar<T>> {
	
	// methods
	// ........................................................................
	
	T zero();
	
	T one();
	
	T negative();
	
	T inverse();
	
	T add(T x);
	
	T mlt(T x);

	// TODO
	//
	//  o Convert the static power method into a default method of
	//    the interface.
	//
	//  o Change the signature of the method, so the value 'x' becomes
	//    the scalar, on which the method is invoked.
	//
	static <T extends Scalar<T>> T pow(T x, int n) {
		
		T y = x.one();
		
		for (int i = 0; i < n; i++) {
			
			y = y.mlt(x);
			
		} // for
		
		return y;

	}

}
