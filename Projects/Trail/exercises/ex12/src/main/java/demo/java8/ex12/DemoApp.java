/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2014 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.java8.ex12;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import demo.util.Demo;

/**
 * The demo application.
 */
public class DemoApp {
	
	// constructors
	// ........................................................................

	private DemoApp() {}

	// methods
	// ........................................................................
	
	private void runDemo() {
		
		Demo.log("Running demo ...");

		this.demoCount();
		this.demoMin();
		this.demoMax();
		this.demoReduce();
		
		Demo.log("Finished.");
		
	}
	
	private void demoCount() {
		
		Demo.log("count():");
		Stream<Integer> stream = this.createStream();
		
		// TODO
		//
		//  o Count the elements of the stream.
		//
		
		Demo.println(2);
		
	}
	
	private void demoMin() {
		
		Demo.log("min(Comparator):");
		Stream<Integer> stream = this.createRandomStream();
		
		// TODO
		//
		//  o Find the minimum value of the stream.
		//
		
		Demo.println(2);
		
	}
	
	private void demoMax() {
		
		Demo.log("max(Comparator):");
		Stream<Integer> stream = this.createRandomStream();
		
		// TODO
		//
		//  o Find the maximum value of the stream.
		//
		
		Demo.println(2);
		
	}
	
	private void demoReduce() {
		
		Demo.log("reduce(BinaryOperator):");
		Stream<Integer> stream = this.createRandomStream();
		
		// TODO
		//
		//  o Calculate the sum of all stream values.
		//
		//    Hint: Use the stream method 'reduce(BinaryOperator)'.
		//
		
		Demo.println(2);
		
	}

	private Stream<Integer> createStream() {
		
		return Stream.of(0, 1, 2, 3, 4, 5, 6, 7, 8, 9);
		
	}

	private Stream<Integer> createRandomStream() {
		
		return Stream.generate(() -> Demo.nextInt(10)).limit(10);
		
	}

	/**
	 * Runs the program.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {
		
		DemoApp app = new DemoApp();
		app.runDemo();
		
	}
	
}
