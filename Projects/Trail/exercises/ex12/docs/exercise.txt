Terminal Stream Operations
==========================

A) Using Stream Operations
==========================

  'DemoApp':
  ----------
  
  'demoCount()':
  --------------
  
  1) Count the elements of the stream.
  
  
  'demoMin()':
  ------------
  
  2) Find the minimum value of the stream.
  
  
  'demoMax()':
  ------------
  
  3) Find the maximum value of the stream.
  
  
  'demoReduce()':
  ---------------
  
  4) Calculate the sum of all stream values.
     
     Hint: Use the stream method 'reduce(BinaryOperator)'.
