/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2014 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.java8.ex14;

import java.util.stream.IntStream;
import java.util.stream.Stream;

import demo.util.Demo;

/**
 * The demo application.
 */
public class DemoApp {
	
	// constructors
	// ........................................................................

	private DemoApp() {}

	// methods
	// ........................................................................
	
	private void runDemo() {
		
		Demo.log("Running demo ...");

		this.demoSequential();
		this.demoParallel();
		this.demoSequentialWithExecutingThread();
		this.demoParallelWithExecutingThread();
		
		Demo.log("Finished.");
		
	}
	
	private void demoSequential() {
		
		Demo.log("Sequential:");
		this.createStream().forEach(x -> Demo.print(x + " "));
		Demo.println(2);
		
	}
	
	private void demoParallel() {
		
		Demo.log("Parallel:");
		
		// TODO
		//
		//  o Create a stream and make it parallel.
		//
		//  o Print the elements of the stream on the console.
		//
		
		Demo.println(2);
		
	}
	
	private void demoSequentialWithExecutingThread() {
		
		Demo.log("Sequential with Thread-Info:");
		
		// TODO
		//
		//  o Create a sequential stream.
		//
		//  o Print the executing thread on the console.
		//
		//    Hint: Use the method 'peek(...)'.
		//
		//  o Print the elements of the stream on the console.
		//
		
		Demo.println(2);
		
	}
	
	private void demoParallelWithExecutingThread() {
		
		Demo.log("Parallel with Thread-Info:");
		
		// TODO
		//
		//  o Create a parallel stream.
		//
		//  o Print the executing thread on the console.
		//
		//    Hint: Use the method 'peek(...)'.
		//
		//  o Print the elements of the stream on the console.
		//
		
		Demo.println(2);
		
	}
	
	private Stream<Integer> createStream() {
		
		return IntStream.range(0, 10).boxed();
		
	}

	/**
	 * Runs the program.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {
		
		DemoApp app = new DemoApp();
		app.runDemo();
		
	}
	
}
