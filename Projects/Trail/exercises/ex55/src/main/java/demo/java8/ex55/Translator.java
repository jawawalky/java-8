/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2016 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.java8.ex55;

import java.util.HashMap;
import java.util.Map;

import demo.util.Demo;

/**
 * A translator.
 * 
 * @author Franz Tost
 */
public class Translator {
	
	// constants
	// ........................................................................
	
	private static final Map<String, String> DICTIONARY = new HashMap<>();

	// static block
	// ........................................................................
	
	static {
		
		DICTIONARY.put("rain", "Regen");
		DICTIONARY.put("sun",  "Sonne");
		DICTIONARY.put("wind", "Wind");
		DICTIONARY.put("snow", "Schnee");
		
	}
	
	// fields
	// ........................................................................
	
	// TODO
	//
	//  o Add an attribute of type 'StampedLock' and call it 'lock'.
	//

	// methods
	// ........................................................................

	public String translate(final String wordInEnglish) {
		
		long startTime = System.currentTimeMillis();
		
		// TODO
		//
		//  o Try to obtain a stamp by an optimistic read operation.
		//
		
		Demo.sleep(500);
		String wordInGerman = DICTIONARY.get(wordInEnglish);
		
		// TODO
		//
		//  o Validate the stamp.
		//
		//  o If the stamp is not valid, then obtain a real read-lock
		//    an re-read the translation. (Simulate a processing time
		//    of 500 milliseconds).
		//
		//  o Unlock the lock in a 'finally'-block.
		//
		
		Demo.log("translate(): " + wordInEnglish + " -> " + wordInGerman + " [" + (System.currentTimeMillis() - startTime) + " ms]");
		return wordInGerman;
		
	}
	
	public void addTranslation(
		final String wordInEnglish,
		final String wordInGerman
	) {
		
		long startTime = System.currentTimeMillis();
		
		// TODO
		//
		//  o Obtain a write-lock and store the stamp in a local variable.
		//
		
		Demo.sleep(2000);
		DICTIONARY.put(wordInEnglish, wordInGerman);
			
		// TODO
		//
		//  o Unlock the lock in a 'finally'-block.
		//
		
		Demo.log("addTranslation: " + wordInEnglish + " -> " + wordInGerman +  " [" + (System.currentTimeMillis() - startTime) + " ms]");
		
	}
	
}
