package demo.java8.ex50;

import demo.util.Demo;

/**
 * Mathematical operations.
 */
public final class Math {
	
	// methods
	// ........................................................................
	
	public static int add(final int a, final int b) {
		
		Demo.sleep(200);
		return a + b;
		
	}
	
	public static int mlt(final int a, final int b) {
		
		Demo.sleep(200);
		return a * b;
		
	}
	
	public static int square(final int x) {
		
		return mlt(x, x);
		
	}
	
	public static int cube(final int x) {
		
		return mlt(x, square(x));
		
	}
	
}
