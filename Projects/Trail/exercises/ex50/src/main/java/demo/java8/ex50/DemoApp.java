/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2014 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.java8.ex50;

import demo.util.Demo;

/**
 * The demo application.
 */
public class DemoApp {
	
	// constructors
	// ........................................................................

	private DemoApp() { }

	// methods
	// ........................................................................
	
	/**
	 * Runs the demo.
	 */
	private void runDemo() {
		
		Demo.log("Running demo ...");
		
		try {
			
			this.computeNormally();
			this.computeSequentially();
			this.computeParallelized();
			
			Demo.log("Finished.");
			
		} // try
		catch (Exception e) {
			
			Demo.log(e);
			
		} // catch
		
	}
	
	private void computeNormally() throws Exception {
		
		// x² + x³
		
		Demo.log("Normal Computation:");
		
		long start = System.currentTimeMillis();
		
		// TODO
		//
		//  o Use the methods of the class 'demo.java8.ex50.Math' to
		//    compute the function f(x) = x² + x³ for x = 2.
		//
		
		Integer result = null;
		
		long end = System.currentTimeMillis();
		
		Demo.log("2² + 2³ = " + result + ", [in " + (end - start) + " ms]");
			
	}
	
	private void computeSequentially() throws Exception {
		
		// x² + x³
		
		Demo.log("Sequential Computation:");
		
		long start = System.currentTimeMillis();
		
		// TODO
		//
		//  o Compute the function f(x) = x² + x³ for x = 2, using
		//    a 'CompletableFuture'.
		//
		//  o Create an instance of 'CompletableFuture' 'f0'.
		//
		//  o Use the method 'thenApply(...)' of 'f0' to calculate
		//    the square of x and assign the returned 'CompletableFuture'
		//    to 'f1'.
		//
		//  o Use the method 'thenApply(...)' of 'f0' to calculate
		//    the cube of x and assign the returned 'CompletableFuture'
		//    to 'f2'.
		//
		//  o Combine 'f1' and 'f2', using an addition. Call the resulting
		//    'CompletableFuture' 'f3'.
		//
		//  o Set the value for x (= 2) on the object 'f0'.
		//
		//  o Retrieve the result of the computation from the object 'f3'.
		//
		
		Integer result = null;
		
		long end = System.currentTimeMillis();
		
		Demo.log("2² + 2³ = " + result + ", [in " + (end - start) + " ms]");
			
	}
	
	private void computeParallelized() throws Exception {
		
		// x² + x³
		
		Demo.log("Parallel Computation:");
		
		long start = System.currentTimeMillis();
		
		// TODO
		//
		//  o Compute the function f(x) = x² + x³ for x = 2, using
		//    a 'CompletableFuture'.
		//
		//  o Create an instance of 'CompletableFuture' 'f0'.
		//
		//  o Use the method 'thenApplyAsync(...)' of 'f0' to calculate
		//    the square of x and assign the returned 'CompletableFuture'
		//    to 'f1'.
		//
		//  o Use the method 'thenApplyAsync(...)' of 'f0' to calculate
		//    the cube of x and assign the returned 'CompletableFuture'
		//    to 'f2'.
		//
		//  o Combine 'f1' and 'f2', using an addition. Call the resulting
		//    'CompletableFuture' 'f3'.
		//
		//  o Set the value for x (= 2) on the object 'f0'.
		//
		//  o Retrieve the result of the computation from the object 'f3'.
		//
		
		Integer result = null;
		
		long end = System.currentTimeMillis();
		
		Demo.log("2² + 2³ = " + result + ", [in " + (end - start) + " ms]");
			
	}
	
	/**
	 * Runs the program.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {
		
		DemoApp app = new DemoApp();
		app.runDemo();
		
	}
	
}
