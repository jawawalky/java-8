Stream Operations
=================

A) Using Stream Operations
==========================

  'DemoApp':
  ----------
  
  'createStream()':
  -----------------
  
  1) Create a stream, which contains all integer values from 0 through 9.
  
  
  'createRandomStream()':
  -----------------------
  
  4) Create a stream, which contains 10 random values.
     Each random value should lie between 0 and 9.
     
     Hint: Use the method 'Demo.nextInt(n)' to create the random value.
     
  
  'demoForEach()':
  ----------------
  
  7) Print each element of the stream on the console.
  
  
  'demoFilter()':
  ---------------
  
  8) Print all even elements of the stream on the console.
  
  
  'demoMap()':
  ------------
  
  9) Map each element of the stream to its double and print
     it on the console.
  
  
  'demoSkip()':
  -------------
  
  10) Skip the first three elements of the stream and print
      the remaining element on the console.
      
      
  'demoLimit()':
  --------------
  
  11) Limit the number of elements of the stream to 5 and print
      them on the console.
      
      
  'demoDistinct()':
  -----------------
  
  12) Print only the distinct values of the stream on the console.
  
  
  'demoSorted()':
  ---------------
  
  13) Sort the values of the stream and print them on the console.
