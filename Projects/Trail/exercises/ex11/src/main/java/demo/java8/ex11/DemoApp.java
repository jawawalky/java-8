/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2014 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.java8.ex11;

import java.util.stream.Stream;

import demo.util.Demo;

/**
 * The demo application.
 */
public class DemoApp {
	
	// constructors
	// ........................................................................

	private DemoApp() {}

	// methods
	// ........................................................................
	
	private void runDemo() {
		
		Demo.log("Running demo ...");
		
		this.demoForEach();
		this.demoFilter();
		this.demoMap();
		this.demoSkip();
		this.demoLimit();
		this.demoDistinct();
		this.demoSorted();
		
		Demo.log("Finished.");
		
	}
	
	private Stream<Integer> createStream() {
		
		// TODO
		//
		//  o Create a stream, which contains all integer values from
		//    0 through 9.
		//
		//  o Returns the stream.
		//
		
		return null;
		
	}

	private Stream<Integer> createRandomStream() {
		
		// TODO
		//
		//  o Create a stream, which contains 10 random values.
		//    Each random value should lie between 0 and 9.
		//
		//    Hint: Use the method 'Demo.nextInt(n)' to create the random
		//          value.
		//
		//  o Returns the stream.
		//
		
		return null;
		
	}

	private void demoForEach() {
		
		Demo.log("forEach(Consumer):");
		Stream<Integer> stream = this.createStream();
		
		// TODO
		//
		//  o Print each element of the stream on the console.
		//
		
		Demo.println(2);
		
	}

	private void demoFilter() {
		
		Demo.log("filter(Predicate):");
		Stream<Integer> stream = this.createStream();
		
		// TODO
		//
		//  o Print all even elements of the stream on the console.
		//
		
		Demo.println(2);
		
	}

	private void demoMap() {
		
		Demo.log("map(Function):");
		Stream<Integer> stream = this.createStream();
		
		// TODO
		//
		//  o Map each element of the stream to its double and
		//    print it on the console.
		//
		
		Demo.println(2);
		
	}

	private void demoSkip() {
		
		Demo.log("skip(int):");
		Stream<Integer> stream = this.createStream();
		
		// TODO
		//
		//  o Skip the first three elements of the stream and print
		//    the remaining element on the console.
		//
		
		Demo.println(2);
		
	}

	private void demoLimit() {
		
		Demo.log("limit(long):");
		Stream<Integer> stream = this.createStream();
		
		// TODO
		//
		//  o Limit the number of elements of the stream to 5 and print
		//    them on the console.
		//
		
		Demo.println(2);
		
	}

	private void demoDistinct() {
		
		Demo.log("distinct():");
		Stream<Integer> stream = this.createRandomStream();
		
		// TODO
		//
		//  o Print only the distinct values of the stream on the console.
		//
		
		Demo.println(2);
		
	}

	private void demoSorted() {
		
		Demo.log("sorted():");
		Stream<Integer> stream = this.createRandomStream();
		
		// TODO
		//
		//  o Sort the values of the stream and print them on the console.
		//
		
		Demo.println(2);
		
	}

	/**
	 * Runs the program.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {
		
		DemoApp app = new DemoApp();
		app.runDemo();
		
	}
	
}
