/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2014 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.java8.ex71;

import demo.util.Demo;

/**
 * The demo application.
 */
public class DemoApp {
	
	// constructors
	// ........................................................................

	private DemoApp() {}

	// methods
	// ........................................................................
	
	/**
	 * Runs the demo.
	 */
	private void runDemo() {
		
		Demo.log("Running demo ...");
		
		// TODO
		//
		//  o Create a 'LocalDate' object, containing the current date.
		//    Print it on the console.
		//
		//  o Parse the date '2013-06-15', store it in a 'LocalDate' object
		//    and print it on the console.
		//
		//  o Create a 'LocalDate' object for the date 17th August 2000.
		//    Print the object on the console.
		//
		//  o Determine the time instants of today's date in the time zones
		//    
		//      o 'America/Chicago',
		//      o the system time zone and
		//      o 'Asia/Singapore'
		//
		//    Print the instants on the console.
		//
		//  o Check the temporal order of the time instants, i.e. which is
		//    the earliest instant, which the second and which is the last.
		//
		
		Demo.log("Finished.");
		
	}
	
	/**
	 * Runs the program.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {
		
		DemoApp app = new DemoApp();
		app.runDemo();
		
	}
	
}
