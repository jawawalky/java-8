Multi-Parameter Lambda Expressions
==================================

A) Creating a Functional Interface
==================================

  1) Create the interface called 'demo.java8.ex02.Operation'.
  
  2) Add the method 'int perform(int, int)'.
  
  3) Why is this interface functional?
  
  
B) Using Lambda Expressions
===========================

  'CalculatorA':
  --------------
  
  1) Add operations for
     
       o Addition
       o Subtraction
       o Multiplication
       o Division
       
     Hint: Use the functional interface 'Operation' and lambda
           expressions.

  2) Create a method 'evaluate(...)', which takes an operation to
     process the two values 'a' and 'b' and returns the result of
     the operation.
     
     
  'add(int, int)':
  ----------------
  
  3) Use the method 'evaluate(...)' and the operation for
     the addition to calculate the result and return it.
     
     
  'sub(int, int)':
  ----------------
  
  4) Use the method 'evaluate(...)' and the operation for
     the subtraction to calculate the result and return it.
     
     
  'mlt(int, int)':
  ----------------
  
  5) Use the method 'evaluate(...)' and the operation for
     the multiplication to calculate the result and return it.
     
     
  'div(int, int)':
  ----------------
  
  6) Use the method 'evaluate(...)' and the operation for
     the division to calculate the result and return it.
     
     
  'CaluclatorB':
  --------------
  
  7) Make a copy of 'CaluclatorA' and call it 'CalculatorB'.
  
  8) Remove the fields for
     
       o Addition
       o Subtraction
       o Multiplication
       o Division
       
     and in-line the lambda expressions into the call of the method
     'evaluate(...)'.
     
     
  'CalculatorC':
  --------------

  9) Make a copy of 'CaluclatorB' and call it 'CalculatorC'.
  
  10) Replace the functional interface 'Operation' by the pre-defined
      interface 'java.util.function.BiFunction'.

      
C) Running the Demo
===================

  'DemoApp':
  ----------
  
  1) Create an instance of 'CalculatorA' and perform some calculations.
  
  2) Create an instance of 'CalculatorB' and perform some calculations.
  
  3) Create an instance of 'CalculatorC' and perform some calculations.
