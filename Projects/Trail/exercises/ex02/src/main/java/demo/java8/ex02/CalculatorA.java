/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2014 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.java8.ex02;

/**
 * A calculator class.
 */
public class CalculatorA {
	
	// fields
	// ........................................................................
	
	// TODO
	//
	//  o Add operations for
	//
	//      o Addition
	//      o Subtraction
	//      o Multiplication
	//      o Division
	//
	//    Hint: Use the functional interface 'Operation' and lambda
	//          expressions.
	//
	
	// constructors
	// ........................................................................
	
	// methods
	// ........................................................................
	
	public int add(int a, int b) {
		
		// TODO
		//
		//  o Use the method 'evaluate(...)' and the operation for
		//    the addition to calculate the result and return it.
		//
		
		return 0;
		
	}
	
	public int sub(int a, int b) {
		
		// TODO
		//
		//  o Use the method 'evaluate(...)' and the operation for
		//    the subtraction to calculate the result and return it.
		//
		
		return 0;
		
	}
	
	public int mlt(int a, int b) {
		
		// TODO
		//
		//  o Use the method 'evaluate(...)' and the operation for
		//    the multiplication to calculate the result and return it.
		//
		
		return 0;
		
	}
	
	public int div(int a, int b) {
		
		// TODO
		//
		//  o Use the method 'evaluate(...)' and the operation for
		//    the division to calculate the result and return it.
		//
		
		return 0;
		
	}
	
	// TODO
	//
	//  o Create a method 'evaluate(...)', which takes an operation to
	//    process the two values 'a' and 'b' and returns the result of
	//    the operation.
	//
	
}
