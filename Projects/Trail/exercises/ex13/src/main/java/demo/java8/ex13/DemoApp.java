/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2014 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.java8.ex13;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import demo.util.Demo;

/**
 * The demo application.
 */
public class DemoApp {
	
	// constructors
	// ........................................................................

	private DemoApp() {}

	// methods
	// ........................................................................
	
	private void runDemo() {
		
		Demo.log("Running demo ...");

		this.demoJoining();
		this.demoGroupingBy();
		this.demoToSet();
		this.demoAveragingInt();
		
		Demo.log("Finished.");
		
	}
	
	private void demoJoining() {
		
		Demo.log("joining(CharSequence):");
		
		// TODO
		//
		//  o Join the strings 'one', 'two' and 'three' with
		//    the delimiter ', ' and print the string on the console.
		//    
		
		Demo.println(2);
		
	}
	
	private void demoGroupingBy() {
		
		Demo.log("groupingBy(Function):");
		
		// TODO
		//
		//  o Group the numbers from 0 through 9 by odd and even numbers.
		//
		//    Hint: Use the enum 'NumberType'.
		//    
		
		Demo.println(2);
		
	}
	
	private void demoToSet() {
		
		Demo.log("toSet():");
		
		// TODO
		//
		//  o Create a stream of 10 random numbers and collect them in a set.
		//
		//  o Print the elements of the set on the console.
		//    
		
		Demo.println(2);
		
	}
	
	private void demoAveragingInt() {
		
		Demo.log("averagingInt(ToIntFunction):");
		
		// TODO
		//
		//  o Create a stream of 10 random numbers and calculate their
		//    average value.
		//
		//  o Print the average on the console.
		//    
		
		Demo.println(2);
		
	}
	
	private Stream<Integer> createStream() {
		
		return Stream.of(0, 1, 2, 3, 4, 5, 6, 7, 8, 9);
		
	}

	private Stream<Integer> createRandomStream() {
		
		return Stream.generate(() -> Demo.nextInt(10)).limit(10);
		
	}

	/**
	 * Runs the program.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {
		
		DemoApp app = new DemoApp();
		app.runDemo();
		
	}
	
}
