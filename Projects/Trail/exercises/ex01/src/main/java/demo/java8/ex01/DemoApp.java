/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2014 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.java8.ex01;

import java.util.ArrayList;
import java.util.List;

import demo.util.Demo;

/**
 * The demo application.
 */
public class DemoApp {
	
	// constructors
	// ........................................................................

	private DemoApp() {}

	// methods
	// ........................................................................
	
	/**
	 * Runs the demo.
	 */
	private void runDemo() {
		
		Demo.log("Running demo ...");
		
		List<Address> addresses = this.createAddresses();
		
		this.printWithAnonymousClass(addresses);
		this.printWithLambdaExpression(addresses);
		this.printWithLambdaExpression2(addresses);
		this.printWithLambdaExpression3(addresses);
		
		Demo.log("Finished.");
		
	}
	
	private List<Address> createAddresses() {
		
		List<Address> addresses = new ArrayList<>();
		
		addresses.add(new Address("Fraunhoferstrasse",    20, "München"));
		addresses.add(new Address("Kaltmühle",            18, "Frankfurt am Main"));
		addresses.add(new Address("Unter den Linden",    112, "Berlin"));
		addresses.add(new Address("Auguststrasse",        11, "Berlin"));
		addresses.add(new Address("Breite Strasse",       97, "Köln"));
		addresses.add(new Address("Königsallee",          44, "Düsseldorf"));
		addresses.add(new Address("Am Platzl",             1, "München"));
		addresses.add(new Address("Holzstrasse",          49, "München"));
		addresses.add(new Address("Kurfürstendamm",      298, "Berlin"));
		addresses.add(new Address("Hoheluftchaussee",    127, "Hamburg"));
		addresses.add(new Address("Am Römerkastell",      36, "Stuttgart"));
		addresses.add(new Address("Leopoldstrasse",      230, "München"));
		addresses.add(new Address("Akademiegarten",       59, "Stuttgart"));
		addresses.add(new Address("Charlottenplatz",       3, "Stuttgart"));
		addresses.add(new Address("Akazienalle",         120, "Essen"));
		addresses.add(new Address("Brookweg",             29, "Münster"));
		addresses.add(new Address("Potgasse",             12, "Dortmund"));
		addresses.add(new Address("Am Plärrer",            1, "Nürnberg"));
		addresses.add(new Address("Lotterstrasse",        69, "Leipzig"));
		addresses.add(new Address("Postplatz",             5, "Dresden"));
		addresses.add(new Address("Terrassenufer",       223, "Dresden"));
		addresses.add(new Address("An der blauen Kappe",  16, "Augsburg"));
		addresses.add(new Address("Graben",               15, "Weimar"));
		addresses.add(new Address("Wiener Platz",          2, "Dresden"));
		addresses.add(new Address("Spittelmarkt",         55, "Berlin"));
		addresses.add(new Address("Zeil",                103, "Frankfurt am Main"));
		addresses.add(new Address("Kleine Freiheit",      10, "Hamburg"));
		addresses.add(new Address("Barfussgässchen",      19, "Leipzig"));
		addresses.add(new Address("Ruppiner Strasse",     20, "Berlin"));
		addresses.add(new Address("Wiener Platz",          1, "München"));
		
		return addresses;
		
	}
	
	// TODO
	// 
	//  o Add a method parameter of type 'demo.java8.ex01.Filter'.
	//
	private void printAddresses(final List<Address> addresses) {
		
		for (Address address : addresses) {
			
			// TODO
			//
			//  o Print only addresses, which pass the filter.
			//
			
		} // for
		
	}

	// TODO
	//
	//  o Add a method parameter of the pre-defined functional interface
	//    'java.util.function.Predicate'.
	//
	private void printAddressesWithStandardFunctionalInterface(
		final List<Address> addresses
	) {
		
		for (Address address : addresses) {
			
			// TODO
			//
			//  o Print only addresses, match the predicate.
			//
			
		} // for
		
	}
	
	private void printWithAnonymousClass(final List<Address> addresses) {
		
		Demo.log("(1) Addresses in Hamburg:");
		
		// TODO
		//
		//  o Print only addresses in Hamburg, using an anonymous inner class,
		//    which implements the 'Filter' interface.
		//
		
	}
	
	private void printWithLambdaExpression(final List<Address> addresses) {
		
		Demo.log("(2) Addresses in Berlin:");
		
		// TODO
		//
		//  o Print only addresses in Berlin, using a lambda expression
		//    on the functional interface 'Filter'.
		//    (full syntax)
		//
		
	}
	
	private void printWithLambdaExpression2(final List<Address> addresses) {
		
		Demo.log("(3) Addresses in Berlin:");
		
		// TODO
		//
		//  o Print only addresses in Berlin, using a lambda expression
		//    on the functional interface 'Filter'.
		//    (simplified syntax)
		//
		
	}
	
	private void printWithLambdaExpression3(final List<Address> addresses) {
		
		Demo.log("(4) Addresses in Berlin:");
		
		// TODO
		//
		//  o Print only addresses in Berlin, using a lambda expression
		//    with the pre-defined functional interface 'Predicate'.
		//
		
	}
	
	/**
	 * Runs the program.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {
		
		DemoApp app = new DemoApp();
		app.runDemo();
		
	}
	
}
