/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2016 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.java8.ex30;

import java.math.BigDecimal;
import java.text.DecimalFormat;

/**
 * A simple implementation of the {@link Scalar} interface.
 * 
 * @author Franz Tost
 *
 */
public class Real implements Scalar<Real> {
	
	// constants
	// ........................................................................
	
	private static final Real ZERO = new Real(BigDecimal.ZERO);
	
	private static final Real ONE  = new Real(BigDecimal.ONE);
	
	private static final DecimalFormat FORMAT = new DecimalFormat("###0.00");
	
	// fields
	// ........................................................................
	
	private BigDecimal value;

	// constructors
	// ........................................................................
	
	public Real(final double value) {
		
		this(new BigDecimal(value));
		
	}
	
	public Real(final BigDecimal value) {
		
		super();
		
		this.value = value;
		
	}
	
	// methods
	// ........................................................................
	
	@Override
	public Real zero() { return ZERO; }

	@Override
	public Real one() { return ONE; }

	@Override
	public Real negative() { return new Real(this.value.negate()); }

	@Override
	public Real inverse() { return new Real(BigDecimal.ONE.divide(this.value)); } 

	@Override
	public Real add(Real x) { return new Real(this.value.add(x.value)); }

	@Override
	public Real mlt(Real x) { return new Real(this.value.multiply(x.value)); }

	@Override
	public String toString() { return FORMAT.format(this.value); }
	
}
