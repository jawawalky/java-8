/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2014 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.java8.ex30;

import demo.util.Demo;

/**
 * The demo application.
 */
public class DemoApp {
	
	// constructors
	// ........................................................................

	/**
	 * A private default constructor.
	 */
	private DemoApp() {}

	// methods
	// ........................................................................
	
	/**
	 * Runs the demo.
	 */
	private void runDemo() {
		
		Demo.log("Running demo ...");
		
		// TODO
		//
		//  o Create some 'Real' number.
		//
		//  o Calculate its 4th power and print it on the console.
		//
		
		Demo.log("Finished.");
		
	}
	
	/**
	 * Runs the program.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {
		
		DemoApp app = new DemoApp();
		app.runDemo();
		
	}
	
}
