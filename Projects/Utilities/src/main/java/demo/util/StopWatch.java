/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2017 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.util;

/**
 * A simple stop watch.
 * 
 * @author Franz Tost
 */
public interface StopWatch {
	
	// inner classes
	// ........................................................................
	
	class SimpleStopWatch implements StopWatch {
		
		// fields
		// ....................................................................
		
		private long startTime;
		
		private long elapsedTime;
		
		// constructors
		// ....................................................................
		
		// methods
		// ....................................................................
		
		@Override public synchronized void start() {
			
			if (this.startTime == 0L) {
				
				this.startTime = System.currentTimeMillis();
				
			} // if
			
		}

		@Override public synchronized void stop() { 
		
			if (this.startTime != 0L) {
				
				this.elapsedTime += (System.currentTimeMillis() - this.startTime);
				this.startTime = 0L;
				
			} // if
		
		}

		@Override public synchronized void reset() {
			
			if (this.startTime == 0L) {
				
				this.elapsedTime = 0L;
				
			} // if
			
		}
		
		@Override public long getElapsedTime() {
			
			if (this.startTime != 0L) {
				
				return this.elapsedTime + (System.currentTimeMillis() - this.startTime);
				
			} // if
			else {
				
				return this.elapsedTime;
				
			} // else
		
		}

	}
	
	class ThreadLocalStopWatch implements StopWatch {
		
		// fields
		// ....................................................................
		
		private ThreadLocal<StopWatch> stopWatch =
				
			new ThreadLocal<StopWatch>() {

				@Override protected StopWatch initialValue() {
					
					return new SimpleStopWatch();
					
				}
			
			};

		@Override public void start() { this.stopWatch.get().start(); }

		@Override public void stop()  { this.stopWatch.get().stop();  }

		@Override public void reset() { this.stopWatch.get().reset(); }

		@Override public long getElapsedTime() {
			
			return this.stopWatch.get().getElapsedTime();
			
		}
		
	}
	
	// methods
	// ........................................................................
	
	void start();

	void stop();

	void reset();
	
	long getElapsedTime();
	
	static StopWatch simpleStopWatch()      { return new SimpleStopWatch();      }

	static StopWatch threadLocalStopWatch() { return new ThreadLocalStopWatch(); }
	
	default void print() {
		
		Demo.log("Elapsed time: %d ms", this.getElapsedTime());
		
	}

	default void print(final String message) {
		
		Demo.log("[%s] after %d ms", message, this.getElapsedTime());
		
	}

}
