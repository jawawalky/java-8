/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2014 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.util;

import java.time.ZoneId;

import demo.util.Demo;

/**
 * The application lists all available time zone IDs.
 */
public class ListZoneIds {
	
	// constructors
	// ........................................................................

	private ListZoneIds() {}

	// methods
	// ........................................................................
	
	/**
	 * Runs the demo.
	 */
	private void runDemo() {
		
		Demo.log("ZoneId Infos:");

		this.showSystemZoneIds();
		this.showAvailableZoneIds();
		
		Demo.log("Finished.");
		
	}
	
	private void showSystemZoneIds() {
		
		Demo.log("  System Zone ID: " + ZoneId.systemDefault());
		
	}
	
	private void showAvailableZoneIds() {
		
		Demo.log("  Available Zone IDs:");
		
		for (String zoneId : ZoneId.getAvailableZoneIds()) {
			
			Demo.log("    - " + zoneId);
			
		} // for
		
	}
	
	/**
	 * Runs the program.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {
		
		ListZoneIds app = new ListZoneIds();
		app.runDemo();
		
	}
	
}
